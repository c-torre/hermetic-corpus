Hermetic Corpus
===============

Hermetic Corpus translated by G.R.S Mead to English.
Chapters I to XIII.

Originally from the [Internet Sacred Text Archive](https://www.sacred-texts.com/chr/herm/) (modified HTM code).
See also the [Gnostic Society Library version](http://www.gnosis.org/library/hermet.htm).

Compiled using a LaTeX engine from HTM using pandoc.
This means finally good enough formatting and PDF headers.

Use
---

Read the PDF with your favorite reader e.g. [Sumatra PDF](https://www.sumatrapdfreader.org/free-pdf-reader.html).

In case you want to recompile the files, you will need `pandoc`, then:

```
$ sh make
```

License
-------

CC0
