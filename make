#!/bin/sh
#
# Merge all HTM files and compile into one PDF

pandoc --toc -V documentclass=report -o corpus-hermeticum.pdf hermes*htm
